import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
   lArtists:[],
   actorInfo:{},
   albumInfo:{},
   during:0,
   composDuration:[],
   albums:[],
   allIsLoaded:false
  },
  mutations: {
    SET_ARTISTS(state, payload){
      state.lArtists.push(payload)
    },
    SET_ALL_DOWNLOADED(state, payload){
      state.allIsLoaded = payload
    },
    SET_ALBUMS(state, payload){
      state.albums = payload
    },
    SET_ALBUM_INFO(state, payload){
      state.albumInfo = payload
    },
    SET_TOTAL_DURING(state, payload){
      if (payload.length > 0){
        state.during = 0
      for (var i = 0; i < payload.length; i++){
         let num = Number(payload[i].duration.replace(':','.')).toFixed(1)
        state.during += Number(num)
      }
    }else {
      state.during = payload
    }
    },
    SET_COMPOS_DURATION(state, payload){
      state.composDuration = payload
    }
  },
  actions: {
   async GET_ARTISTS({commit} ){
      try {
        let response = await fetch('http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2')
        commit('SET_ARTISTS', await response.json())
        commit('SET_ALL_DOWNLOADED', true)
      } catch (e){
        commit('',e)
      }
    },
    async GET_ALBUMS({commit}, artName){
      try {
        let response = await fetch('http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2')
        let artData = await response.json();
        let albumData = await artData.filter(el => el.name === artName);
        commit ('SET_ALBUMS', albumData[0].album)
      } catch (e){
        commit('',e)
      }
    },
    async GET_DESC({commit},{0:artName, 1:artAlbum}){
      try {
        let response = await fetch('http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2')
        let artData = await response.json();
        let albumData = await artData.filter(el => el.name === artName);
        let compositData = await albumData[0].album.filter(el => el.name === artAlbum);
        commit('SET_ALBUM_INFO', compositData[0])
        commit('SET_COMPOS_DURATION', compositData[0].compositions)
      } catch (e){
        commit('',e)
      }
    },
  },
  modules: {
  }
})
