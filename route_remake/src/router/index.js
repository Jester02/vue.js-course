import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ItemAlbum from '../views/ItemAlbum.vue'
import OutData from '../views/OutData.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/Contacts',
    name: 'Contacts',
    components:{ 
      default: () => import ('../views/Contacts.vue'),
      navigation: () => import('../views/MenuForProject.vue')
    }
  },
  {
    path: '/List',
    name: 'List',
    props: { default: true, navigation:false},
    components:{ 
      default: () => import ('../views/List.vue'),
      navigation: () => import('../views/MenuForProject.vue'),
    }
  },
  {
    path: '/List/:item',
    name: 'Item',
    props:{default:true, navigation:false},
    components:{ 
      default: () => import ('../views/Item.vue'),
      navigation: () => import('../views/MenuForProject.vue'),
    },
    children:[
      {
      name:'album',
      path:':name',
      props: true,
      component: ItemAlbum
      }
    ]
  },
  {
    path: '/',
    name: 'Home',
    components:{ 
      default: () => import ('../views/Home.vue'),
      navigation: () => import('../views/MenuForProject.vue')
    }
  },
  {
    path: '/about',
    name: 'About',
    components:{ 
      default: () => import ('../views/About.vue'),
      navigation: () => import('../views/MenuForProject.vue')
    }
    
  },
  {
    path:'/outdata',
    name: 'outdata',
    component:OutData
  }
  

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
