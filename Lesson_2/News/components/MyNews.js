Vue.component ('news', {
	props : {
		newsShow: {
			type: Object,
			required: true
		}	
	},
	template:
	`<article>
		<img :src='newsShow.img'/>
		<span>{{newsShow.title}}</span>
		<p v-text="newsShow.desc"></p>
		<a :href='newsShow.link'>Show</a>
	</article>`
})
