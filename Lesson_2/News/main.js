const newsArr = [
    {
        id: 1,
        title: 'news 1',
        desc: 'description of news 1',
        img: 'https://media.wsls.com/photo/2017/04/24/Whats%20News%20Today_1493062809311_9576980_ver1.0_1280_720.png',
        link: '#'
    },
    {
        id: 2,
        title: 'news 2',
        desc: 'description of news 2',
        img: '',
        link: '#'
    },
    {
        id: 3,
        title: 'news 3',
        desc: 'description of news 3',
        img: 'https://www.ctvnews.ca/polopoly_fs/1.4344128.1553095687!/httpImage/image.png_gen/derivatives/landscape_620/image.png',
        link: '#'
    }
];
const MyAppNews = new Vue ({
	el:'#app',
	data :{
		newsArr
	}
})
