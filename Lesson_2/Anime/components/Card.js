const Card = {
	props:{
		anime:{ 
		type:Object,
		required:true,
		}
	},
	template:
	`
	<article>
	<img class="_preImg" :src="anime.image_url">
	<span class="_preInfo">#{{anime.mal_id}}</span>
	<span class="_preInfo">{{anime.title}}</span>
	<span class="_preInfo">Episodes {{anime.episodes}}</span>
	<a :href="anime.url">Watch</a>
	</article>
	`
}
