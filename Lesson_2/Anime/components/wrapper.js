const wrapper =  {
	components: {
		load:Loadscreen,
		errscreen:error,
		preAnime:Card,
		btn:Btn,
		noCoin:failure,
	},
	props:['title'],
	data(){
		return {
			id:null,
			unfiltered:[],
			filtered:[],
			loading:false,
			fatalerror:false,
			outdata:false,
		}
	},
	created(){
		this.gettingAnime();
	},
	watch:{
		id(item){
			if (!item) return;
			this.filtered = this.unfiltered.filter(num => {
				if (num.mal_id.toString().indexOf(item.toString()) !== -1) return num
			})
			this.outdata = this.filtered.length === 0;
		}
	},
	methods: {
		gettingAnime (){	
			const MyApp = this;
			let	url=' https://api.jikan.moe/v3/top/anime';
			let options = {
			method:'GET'
			};
			const Sorting = (data) => {
				MyApp.unfiltered = data.top;
				MyApp.filtered = MyApp.unfiltered;
				MyApp.loading = true;;
				if (MyApp.unfiltered.length == 0) MyApp.fatalerror = true
			}
		setTimeout( () => {
		fetch(url)
		.then(res => res.json())
		.then( Sorting )
		.catch(error => {
			MyApp.loading = true;
			MyApp.fatalerror = true
			})
		  }, 2000)
		},
		loadTiming() {
			this.loading = false;
			this.gettingAnime()
		},
		clearPage () {
			this.unfiltered = [];
			this.filtered = [];

		}
	},
	computed:{
		myTop(){
			return this.unfiltered.slice(0, 10);
		},
		reverse(){
			return this.filtered.reverse();
		},
		typeMovie(){
			return this.unfiltered.filter(item => item.type == 'Movie')
 		},
		typeTV(){
			return this.unfiltered.filter(item => item.type == 'TV')
		},
		deleteById(){
			this.unfiltered = this.unfiltered.filter(item => item.mal_id !== this.id)
			this.filtered = this.unfiltered;
			this.id = null;
		}
		},

	template:
	`
	<div class="_AnimeBlock">
	<div class="_MyTitle">{{title}}</div>
	<template v-if="loading">
	<div class="_filterTitles">
	<div class="_titles">Filters</div>
	<div class="_idFilter">Filtring by id</div>
	</div>
	<div class="_header">
		<div class="_download">
			<btn class="_MyBtn" label="Delete All Anime :c" v-on:click.native="clearPage" v-show="unfiltered.length > 0"></btn>
			<btn class="_MyBtn" label="Download Anime <3" v-on:click.native="loadTiming" v-show="unfiltered.length === 0"></btn>
		</div>
		<div class="_filtring">
			<btn class="_fBtn" label="Top 10" v-on:click.native="filtered = myTop"></btn>
			<btn class="_fBtn" label="Reverse" v-on:click.native="filtered = reverse"></btn>
			<btn class="_fBtn" label="Movie" v-on:click.native="filtered = typeMovie"></btn>
			<btn class="_fBtn" label="Titles" v-on:click.native="filtered = typeTV"></btn>
		</div>
		<div class="_searching">
		<input type="text" v-model.number.trim="id">
		<btn class="_MyBtn" :label="'Delete By ID #'+this.id" v-on:click.native="deleteById" v-show="id && filtered.length > 0"></btn>
		</div>
	</div>
	</template>
	<load v-show="!loading"></load>
	<errscreen v-if="fatalerror"></errscreen>
	<no-coin v-else-if="outdata"></no-coin>
	<template v-else>
		<div class="_Anime">
			<pre-anime v-for="i in filtered" :anime="i"></pre-anime>
		</div>
	</template>
	</div>
	`,
	}
