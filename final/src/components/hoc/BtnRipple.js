export const BtnRipple = Btn => {
    return {
        functional: true,
        name: 'BtnRipple',
        props: {
            rippleColor: {
                type: String,
                default: 'rgba(255, 255, 255, 0.35)'
            }
        },
        render(h, context) {

            return h(
                Btn,
                {
                    ...context.data,
                    directives: [
                        {
                            name: 'ripple',
                            value: context.props.rippleColor
                        }
                    ],
                    nativeOn: {
                        click: () => {
                            console.log('hi')
                        }
                    }
                },
                context.children
            )
        }
    }
}