module.exports = {
    css: {
        sourceMap: true,
        loaderOptions: {
            sass: {
                prependData: `@import "~@/assets/sass/_vars.sass";`
            }
        //    scss: {}
        //    less: {}
        }
    }
}