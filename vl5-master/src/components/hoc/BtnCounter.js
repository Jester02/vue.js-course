import Vue from 'vue'

export const BtnCounter = Btn => {
    return {
        name: 'BtnCounter',
        data() {
            return {
                counter: 1
            }
        },
        methods: {
            count() {
                this.counter++

            }
        },
        render(h) {
            const self = this
            // debugger
            return h(
                Btn,
                {
                    nativeOn: {
                        click: () => {
                            this.count()
                            console.log(this.counter)
                        }
                    }
                },
                this.$slots.default
            )

        }
    }

}