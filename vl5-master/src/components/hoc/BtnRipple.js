export const BtnRipple = Btn => {
    return {
        functional: true,
        name: 'BtnRipple',
        props: {
            rippleColor: {
                type: String,
                default: 'rgba(255, 255, 255, 0.35)'
            }
        },
        // context передается втолько в случаи если компонент функциональный (functional: true),
        // если компонент обьект - используется область обьекта (this)
        render(h, context) {

            return h(
                Btn,
                {
                    ...context.data,
                    directives: [
                        {
                            name: 'ripple',
                            value: context.props.rippleColor
                        }
                    ],
                    nativeOn: {
                        click: () => {
                            console.log('hi')
                        }
                    }
                },
                context.children
            )
        }
    }
}