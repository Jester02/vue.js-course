import Vue from 'vue'
import Vuex from 'vuex'

import SinglePage from './singlePage'

Vue.use(Vuex)

export default new Vuex.Store({
  /* обьект-хранилище состояния приложения, является реактивным как data в компонентах */
  state: {
    allNews: [],
    singleNews: {},
    isAllLoaded: false,
    onLoadError: '',
    isSingleLoaded: false,
    allReadNews:[]
  },

  /* методы для изменения состояния. Состояние на прямую менять нельзя.
  Мутации не могут быть асинхронными */
  mutations: {
    SET_NEWS(state, payload) {
      state.allNews = payload
    },
    SET_READ_NEWS (state, payload){
      state.allReadNews.push(payload)
    },
    SET_SINGLE_NEWS(state, payload) {
      state.singleNews = payload
    },

    SET_ALL_IS_LOADED(state, payload) {
      state.isAllLoaded = payload
    },

    SET_SINGLE_IS_LOADED(state, payload) {
      state.isSingleLoaded = payload
    },
    SET_ERROR(state, payload) {
      state.onLoadError = payload
    }
  },

  /* в экшинах описывается логика - например запросы к серверу. Экшины могут быть асинхронными */
  actions: {
    async GET_NEWS({ commit }) { /* {commit, state, dispatch} - получается с обьекта store. dispatch - наподобе commit только для actions */
      try {
        let resp = await fetch('http://www.json-generator.com/api/json/get/cgdhmXtiHm?indent=2')

        /* мутации вызываются при помощи commit */
        commit('SET_NEWS', await resp.json()) /* в commit передается имя мутации остальные аргументы это данные которые хоти записать в состояние */
        commit('SET_ALL_IS_LOADED', true)
      } catch (e) {
        commit('', e)
      }

    },
    async GET_SINGLE_NEWS({ commit }, newsID) {
      try {
      let resp = await fetch('http://www.json-generator.com/api/json/get/ceBZIJmoky?indent=2')
      let news = await resp.json()
      let single = await news.filter(item => item.id === parseInt(newsID))
      commit('SET_SINGLE_NEWS', await single)
      commit('SET_SINGLE_IS_LOADED', true)
      } catch (e) {
        commit('', e)
      }
    }
  },

  /* похожи на computed, они так же кешируются. */
  getters: {
    GET_NEWS_WITH_IMG: ({ allNews }) => allNews.filter(el => el.img),
    GET_STATYS_NEWS: ({allReadNews}) => (id)=> allReadNews.indexOf(id)!== -1
  },

  /* если приложение большое, тогда лучше состояние разбить на модули. Например, одна страница один модуль(отдельное состояние)
  * по умолчанию модули регистрируются глобально, но им можно добавить пространство имен namespaced: true,
  *
  * */
  modules: {
    single: SinglePage
  }
})

/* пример добавления модулей */
// const moduleA = {
//   state: { ... },
//   mutations: { ... },
//   actions: { ... },
//   getters: { ... }
// }
//
// const moduleB = {
//   state: { ... },
//   mutations: { ... },
//   actions: { ... }
// }
//
// const store = new Vuex.Store({
//   modules: {
//     a: moduleA,
//     b: moduleB
//   }
// })
//
// store.state.a // -> состояние модуля `moduleA`
// store.state.b // -> состояние модуля `moduleB`
