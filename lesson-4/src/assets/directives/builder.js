export default function builder (el , binding){
    const {mod} = binding.value
    el.classList.add(mod);
}