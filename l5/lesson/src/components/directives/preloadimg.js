const preload = "https://ak.picdn.net/shutterstock/videos/1020134473/thumb/1.jpg";

export default  {
    
    bind(el,binding){
        el.src = preload
    },
    inserted(el, binding){
        
        var observer = new IntersectionObserver(()=> {
            el.src = binding.value;
        });
        observer.observe(el)
    },
    
}