import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    num:0
  },
  mutations: {
    SET_INCREASE(state, payload){
      debugger
      state.num = payload
    }
  },
  actions: {
    COUNT(context){
      context.commit('SET_INCREASE', context.state.num + 1)
    }
  },
  modules: {
  }
})
