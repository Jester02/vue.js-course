import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    verification:false,
    currList:[],
    listAllDos:[],
    gender:null,
    user:null,
    anothPos:false,
  },
  mutations: {
    SET_VERIFICATION(state, payload){
      state.verification = payload
    },
    SET_ROWPOS(state){
      if (state.anothPos == false){
        state.anothPos = true
      } else {
        state.anothPos = false
      }
    },
    SET_GENDER(state, payload){
      state.gender = payload
    },
    SET_USER(state, payload){
      state.user = payload
    },
    SET_CURR_LIST(state, payload){
      state.currList = payload
    },
    SET_LIST_ALL_DOS(state, payload){
      state.listAllDos = payload
    },
    SET_NEWDO_LIST(state, payload){
      state.currList.push(payload)
    },
    SET_NEWDO_ALLDOS(state, payload){
      state.listAllDos.push(payload)
    }
  },
  actions: {
     GET_VERIFY({commit}, {login:vName, password:vPass}){
      let Data = sessionStorage.getItem('Peoples');
      let arr = [];
      let Income = JSON.parse(Data);
      let Checking = Income.filter(el => el.name == vName && el.password == vPass);
      if (sessionStorage.getItem(vName) == null){
        sessionStorage.setItem(vName, JSON.stringify(arr))
        let income = sessionStorage.getItem(vName)
        let tIncome = sessionStorage.getItem('AllDo')
        commit('SET_CURR_LIST', JSON.parse(income))
        commit('SET_LIST_ALL_DOS', JSON.parse(tIncome))
        commit('SET_GENDER', Checking[0].gender)
        commit('SET_USER', vName);
      } else if (sessionStorage.getItem(vName) != null){
        let tIncome = sessionStorage.getItem('AllDo')
        let income = sessionStorage.getItem(vName)
        commit('SET_LIST_ALL_DOS', JSON.parse(tIncome))
        commit('SET_CURR_LIST', JSON.parse(income))
        commit('SET_GENDER', Checking[0].gender)
        commit('SET_USER', vName);
      }
    },
     GET_NEWDO_LIST({commit}, newDo){
       commit('SET_NEWDO_LIST', newDo);
       commit('SET_NEWDO_ALLDOS', newDo); 
       sessionStorage.setItem(this.state.user, JSON.stringify(this.state.currList));
       sessionStorage.setItem('AllDo', JSON.stringify(this.state.listAllDos));
     },
     GET_DELETE_BY_ID({commit},id){
        let Filtered = this.state.currList.filter(el => el.id !== id);
        commit('SET_CURR_LIST', Filtered);
        sessionStorage.setItem(this.state.user, JSON.stringify(Filtered))
     },
     GET_SAVE_DATA({commit}, {sId:id,sItem:item}){
      this.state.currList.forEach(el => {
        if(el.id === id) {
          el.edited = true
          el.changed = true
            Object.keys(item).forEach(key => {
                el[key] = item[key]
            })
        }
      })
      const Obj = this.state.listAllDos.filter(el => el.id === id)
      Obj[0].changed = true;
      commit('SET_CURR_LIST', this.state.currList);
      commit('SET_LIST_ALL_DOS', this.state.listAllDos)
      sessionStorage.setItem(this.state.user, JSON.stringify(this.state.currList));
      sessionStorage.setItem('AllDo', JSON.stringify(this.state.listAllDos))
     },
     GET_DONE_DATA({commit}, id){
       let task_1 = this.state.currList.filter(el => el.id === id)
       let task_2 = this.state.listAllDos.filter(el => el.id == id)
       task_1[0].state = true
       task_2[0].state = true
       sessionStorage.setItem(this.state.user, JSON.stringify(this.state.currList));
       sessionStorage.setItem('AllDo', JSON.stringify(this.state.listAllDos))
     }
  },
  modules: {
  }
})
