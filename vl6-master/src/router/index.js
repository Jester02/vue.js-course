
// добавляем зависимости
import Vue from 'vue'
import VueRouter from 'vue-router'

// Vue-router это плагин, для использования плагинов используется метод Vue.use(plugin)
Vue.use(VueRouter)

// компонент страница
import Home from '@/views/Home.vue'
import p404 from '@/views/p404'

// обьект обьявленных роутов
import routerRoutes from './routes'
import newsRoutes from './news'

// собираем все в кучу
const routes = [
  ...routerRoutes,
  ...newsRoutes,
  {
    path: '/',
    name: 'home',
    component: Home,
    // meta: {....} - метаданные для роута, не мета теги!!!
    // lazy-load для роутов, компонент подтянется только при переходе на страницу
    // component: () => import(/* webpackChunkName: "about" */ '../views/router/About.vue')
  },

  /*  '*' звездочка - принимает лбой роут не подошедший к другим.  */
  {
    path: '*',
    component: p404
  },

  /* Звездочку также можна использовать для динамических путей,
  *  /post-test - часть пути "-test" будет в $route.params.pathMatch */
  {
    path: '/post-*',
    component: Home
  },
]

// Для сножных схем маршутрутизации можно использовать регулярные выражения, но этот модуль( path-to-regexp) имеет свои ограничения

const router = new VueRouter({
  // history - для работы в этом режиме Браузер должен поддерживать HTML5 History.
  // Все современные браузеры поддерживают этот режим, IE с 10 версии
  // режим hash - используются "#хеши" в урле - https://somesite.com/development/520/#cut=undefined/tab=tickets,
  // у поисковых систем проблема с такими урлами и не только у них
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// глобальный хук жизненного цикла
router.beforeEach((to, from, next) => {
  console.log('beforeEach')
  next()
})
router.afterEach((to, from) => {
  console.log('afterEach')
})

export default router
