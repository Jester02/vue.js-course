let int = ['Puck', 'Invoker', 'Zeus'];
let str = ['Bristleback', 'Lifestealer','Huskar'];
let agi = ['Rikimaru', 'Antimage','Pangolier'];
const hero_block = document.getElementById('heroes');
const hero_attribute = document.getElementById('attributes');
const main_stats = document.getElementById('states');

function ConditionShowing (e) {
	for (var i = 0; i<e.length; i++ ){
		var hero_name = document.createElement('li');
		hero_name.innerText = e[i];
		hero_name.className = "trans-listing";
		hero_block.appendChild(hero_name);
	}
}

function ShowNames (e) {
	hero_block.innerHTML=null;
	let arr = e.target.id;
	if (arr == 'int'){
		ConditionShowing(int);
	} else if (arr == 'str'){
		ConditionShowing(str);
	} else if (arr == 'agi'){
		ConditionShowing(agi);
	}
}

function ChangeColor (e){
	let attr = e.target.id;
	console.log(attr);
	if (attr){
		hero_block.className = attr;
	}
}


const MyBtns = hero_attribute.querySelectorAll('._btn');
	  MyBtns.forEach(function(item){
	  	item.addEventListener('click',ShowNames);
	  })

const bgColors = main_stats.querySelectorAll('._btn');
	  bgColors.forEach(function(atr){
	  	atr.addEventListener('click', ChangeColor);
	  })
